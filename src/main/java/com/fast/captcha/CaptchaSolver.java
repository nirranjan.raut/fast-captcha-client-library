package com.fast.captcha;

import com.fast.captcha.model.FastCaptchaResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.*;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class CaptchaSolver {

    private static final String URL = "https://thedataextractors.com/fast-captcha";
    private static final OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(110, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build();

    public static FastCaptchaResponse solve(String fastCaptchaAPIKey, String siteKey, String website) throws Exception {

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody requestBody = RequestBody.create(mediaType, "webUrl=" + website + "&websiteKey=" + siteKey);
        Request request = new Request.Builder()
                .url(URL + "/api/solve/recaptcha")
                .method("POST", requestBody)
                .addHeader("apiSecretKey", fastCaptchaAPIKey)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Cache-Control", "no-cache")
                .build();
        Response response = client.newCall(request).execute();
        int code = response.code();
        System.out.println("Response code " + code);
        ResponseBody responseBody = response.body();
        String body = responseBody.string();
        System.out.println(body);
        Gson gson = new GsonBuilder().create();
        FastCaptchaResponse fastCaptchaResponse = gson.fromJson(body, FastCaptchaResponse.class);
        System.out.println("Response : " + fastCaptchaResponse);
        return fastCaptchaResponse;
    }

    public static FastCaptchaResponse solve(String fastCaptchaAPIKey, File file) throws Exception {
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file",file.getName(),
                        RequestBody.create(MediaType.parse("application/octet-stream"), file))
                .build();
        Request request = new Request.Builder()
                .url(URL + "/api/solve/image")
                .method("POST", requestBody)
                .addHeader("apiSecretKey", fastCaptchaAPIKey)
                .addHeader("Cache-Control", "no-cache")
                .build();
        try (Response response = client.newCall(request).execute()) {
            int code = response.code();
            System.out.println("Response code " + code);
            ResponseBody responseBody = response.body();
            if(responseBody == null || code != 200) {
                throw new RuntimeException("Server error. Contact admin.");
            }
            String body = responseBody.string();
            System.out.println(body);
            Gson gson = new GsonBuilder().create();
            FastCaptchaResponse fastCaptchaResponse = gson.fromJson(body, FastCaptchaResponse.class);
            System.out.println("Response : " + fastCaptchaResponse);
            return fastCaptchaResponse;
        }
    }

    public static FastCaptchaResponse balance(String fastCaptchaAPIKey) throws Exception {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(URL + "/api/balance")
                .method("GET", null)
                .addHeader("apiSecretKey", fastCaptchaAPIKey)
                .addHeader("Cache-Control", "no-cache")
                .build();
        try (Response response = client.newCall(request).execute()) {
            int code = response.code();
            ResponseBody responseBody = response.body();
            if(code != 200 || responseBody == null) {
                throw new RuntimeException("Server error. Contact admin.");
            }
            String body = responseBody.string();
            System.out.println(body);
            Gson gson = new GsonBuilder().create();
            FastCaptchaResponse fastCaptchaResponse = gson.fromJson(body, FastCaptchaResponse.class);
            System.out.println("Response : " + fastCaptchaResponse);
            return fastCaptchaResponse;
        }
    }

}
